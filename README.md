A web app that allows users to set up escrow contract on RIDE language with several clicks, using Waves Keeper.

## Development

Run:  
`npm i`  
`npm run start-dev`

This will launch a Webpack development server, rebuilding the project
automatically each time the sources are updated.

## Production
Run:  
`npm i`  
`npm run build`  
`npm run start` 
 
This will build the project in production mode and launch the Express server on port 3000.

